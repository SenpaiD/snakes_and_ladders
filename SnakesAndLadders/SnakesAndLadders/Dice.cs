﻿using System;

namespace SnakesAndLadders
{
    public class Dice
    {
        static Random r = new Random();

        public static int Roll()
        {
            return r.Next(1, 7);
        }
    }
}