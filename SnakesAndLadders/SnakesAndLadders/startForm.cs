﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakesAndLadders
{
    public partial class startForm : Form
    {
        SaLGame game;
        public startForm()
        {
            InitializeComponent();
        }

        private void startForm_Load(object sender, EventArgs e)
        {
            game = new SaLGame();
            game.StartGame(this);
        }
       internal void ShowPlayer(Player player)
        {
            game.board.cellList[player.position].
                square.Controls.
                Add(new Label { Text = player.name });
         
        }
    }
}
