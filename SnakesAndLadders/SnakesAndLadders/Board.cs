﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SnakesAndLadders
{
    public class Board
    {
        public List<Cell> cellList = new List<Cell>();
        public Board()
        {
            cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));
        }

        public void Show(Form f)
        {
            foreach(Cell cell in cellList)
            {
                cell.Show(f);
            }
            f.Refresh();
        }
    }
}