﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SnakesAndLadders
{
   

    public class Cell
    {
        public int x, y;
       public Panel square;
        public String img;
       public void Show(Form f)
        {
            square = setSquareProperties();
            f.Controls.Add(square);
        }
        private Panel setSquareProperties()
        {
            
            Panel squarePanel = new Panel
            {
                BorderStyle = BorderStyle.None,
                BackgroundImage= Image.FromFile(img),
                Top = x,
                Left=y,
                Height=Constants.cellHeight,
                Width=Constants.cellWidth
            };
            return squarePanel;
        }

    }
}