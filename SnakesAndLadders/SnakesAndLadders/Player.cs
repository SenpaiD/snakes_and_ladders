﻿namespace SnakesAndLadders
{
     class Player
    {
        public string name;
        public int position;
        public Player(string name)
        {
            this.name = name;
            position = 99;
        }
        public Player()
        {
            name = "Default";
            position = 0;
        }
        public void Show(startForm f)
        {
            f.ShowPlayer(this);
        }
    }
}